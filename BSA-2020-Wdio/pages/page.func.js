class PageFunc {
    get target() { return $('.form-control') }
    get result() { return $('#key') }

    /**
     * Clicks the target input field
     */
    clickTarget() {
        this.target.waitForDisplayed()
        this.target.click()
    }

    /**
     * Send keybord keys to Target
     * @param {String} text The keyboard text to enter
     */
    sendKeysToTarget(text) {
        this.target.waitForDisplayed()
        this.target.keys(text)
    }

    /**
     * return the text of the return element
     */
    getResultText() {
        this.result.waitForDisplayed()
        return this.result.getText()
    }


    get minWait() { return $('input#min_wait')}
    get maxWait() { return $('input#max_wait')}
   
    get titleTrigger() { return $('button#page_title_trigger')}

    clickTitleTrigger() {
        this.titleTrigger.waitForDisplayed()
        this.titleTrigger.click()
    }    


    get textValueTrigger() { return $('button#text_value_trigger')}

    clickTextValueTrigger() {
        this.textValueTrigger.waitForDisplayed()
        this.textValueTrigger.click()
    }    

    //
    get buttonVisibilityTrigger() { return $('button#visibility_trigger')}
    
    clickVisibilityTrigger() {
        this.buttonVisibilityTrigger.waitForDisplayed()
        this.buttonVisibilityTrigger.click()
    } 

    //
    get buttonInVisibilityTrigger() { return $('button#invisibility_trigger')}
    
    clickInVisibilityTrigger() {
        this.buttonInVisibilityTrigger.waitForDisplayed()
        this.buttonInVisibilityTrigger.click()
    }       

    //
    get buttonEnableTrigger() { return $('button#enabled_trigger')}
    
    clickEnableTrigger() {
        this.buttonEnableTrigger.waitForDisplayed()
        this.buttonEnableTrigger.click()
    }    

    get buttonWaitFrameTrigger() { return $('button#wait_for_frame')}
    
    clickWaitFrameTrigger() {
        this.buttonWaitFrameTrigger.waitForDisplayed()
        this.buttonWaitFrameTrigger.click()
    } 
      
    get iframe() { return $('#frame #frm')}
    get buttonInnerFrame() { return $('button#inner_button')}
    
    clickFrameInnerButton() {
        this.buttonInnerFrame.waitForDisplayed()
        this.buttonInnerFrame.click()
    }    

    get javascriptAlertButton() { return $('#alert_trigger') }
    
    clickJavascriptAlertButton() {
        this.javascriptAlertButton.waitForDisplayed()
        this.javascriptAlertButton.click()
    }

    getAlertTextResult() {
        return $('#alert_handled').getText()
    } 

    getPromptTextResult(text){
        if (text ==1) return $('#confirm_ok').getText()
        return $('#confirm_cancelled').getText()
    }

    get javascriptPromptButton() { return $('#prompt_trigger') }
    
    clickJavascriptPromptButton(){
        this.javascriptPromptButton.waitForDisplayed()
        this.javascriptPromptButton.click()       
    }
  
}
module.exports = new PageFunc()