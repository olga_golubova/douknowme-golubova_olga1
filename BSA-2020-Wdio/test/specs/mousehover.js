const assert = require('assert');

describe('dineshvelhal mouse hover', () => {
    it ('should mouse hover',() => {
        browser.url('https://dineshvelhal.github.io/testautomation-playground/mouse_events.html');
        $('.dropdown').moveTo();
        browser.pause(5000);
    });

    it ('should hover java',() => {
        $('#dd_java').moveTo();
        $('#dd_java').click();
        const textResult = $('#hover_validate').getText()
        assert.equal(textResult, 'Java')
        browser.pause(5000);
    });

    it ('should hover Python',() => {
        $('#dd_python').moveTo();
        $('#dd_python').click();
        const textResult = $('#hover_validate').getText()
        assert.equal(textResult, 'Python')
        browser.pause(5000);
    });

    it ('should hover javascript',() => {
        $('#dd_javascript').moveTo();
        $('#dd_javascript').click();
        const textResult = $('#hover_validate').getText()
        assert.equal(textResult, 'JavaScript')
        browser.pause(5000);
    });

});