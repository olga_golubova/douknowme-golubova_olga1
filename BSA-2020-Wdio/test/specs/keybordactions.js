pageFunc = require('../../pages/page.func')
const assert = require('assert');

describe('dineshvelhal Keyboard Actions', () => {  
    it ('Ctrl press',() => {
        browser.url('https://dineshvelhal.github.io/testautomation-playground/keyboard_events.html');
        const buttontext = "Control"
        pageFunc.clickTarget()
        pageFunc.sendKeysToTarget(buttontext)
        assert.equal(buttontext, pageFunc.getResultText())    
        browser.pause(2000)
    });

    it ('Alt press',() => {
        browser.url('https://dineshvelhal.github.io/testautomation-playground/keyboard_events.html');
        const buttontext = "Alt"
        pageFunc.clickTarget()
        pageFunc.sendKeysToTarget(buttontext)
        assert.equal(buttontext, pageFunc.getResultText())    
        browser.pause(2000)
    });

    it ('Shift press',() => {
        browser.url('https://dineshvelhal.github.io/testautomation-playground/keyboard_events.html');
        const buttontext = "Shift"
        pageFunc.clickTarget()
        pageFunc.sendKeysToTarget(buttontext)
        assert.equal(buttontext, pageFunc.getResultText())    
        browser.pause(2000)
    });

    
    it ('Home press',() => {
        browser.url('https://dineshvelhal.github.io/testautomation-playground/keyboard_events.html');
        const buttontext = "Home"
        pageFunc.clickTarget()
        pageFunc.sendKeysToTarget(buttontext)
        assert.equal(buttontext, pageFunc.getResultText())    
        browser.pause(2000)
    });

    it ('Meta press',() => {
        browser.url('https://dineshvelhal.github.io/testautomation-playground/keyboard_events.html');
        const buttontext = "Meta"
        pageFunc.clickTarget()
        pageFunc.sendKeysToTarget(buttontext)
        assert.equal(buttontext, pageFunc.getResultText())    
        browser.pause(2000)
    });

    it ('Down press',() => {
        browser.url('https://dineshvelhal.github.io/testautomation-playground/keyboard_events.html');
        const buttontext = "PageDown"
        pageFunc.clickTarget()
        pageFunc.sendKeysToTarget(buttontext)
        assert.equal(buttontext, pageFunc.getResultText())    
        browser.pause(2000)
    });

    it ('PageUp press',() => {
        browser.url('https://dineshvelhal.github.io/testautomation-playground/keyboard_events.html');
        const buttontext = "PageUp"
        pageFunc.clickTarget()
        pageFunc.sendKeysToTarget(buttontext)
        assert.equal(buttontext, pageFunc.getResultText())    
        browser.pause(2000)
    });

});