const assert = require('assert');
pageFunc = require('../../pages/page.func')

describe('dineshvelhal expected condition', () => {

    it('Wait for alert to be present/allert show', () => {
        browser.url('https://dineshvelhal.github.io/testautomation-playground/expected_conditions.html');
        pageFunc.clickJavascriptAlertButton()
        browser.waitUntil(function () {
            return browser.isAlertOpen()
         }, {
             timeout: pageFunc.maxWait,
             timeoutMsg: 'Wait for alert to be present/allert show'
         });
         browser.pause(2000); 
    }); 
 
    it('should accept alert', () => {
        browser.acceptAlert()
        assert.equal('Alert handled', pageFunc.getAlertTextResult())        
        browser.pause(2000); 
    }); 

    it('Wait for alert to be present/prompt show', () => {
        browser.url('https://dineshvelhal.github.io/testautomation-playground/expected_conditions.html');
        pageFunc.clickJavascriptPromptButton()
        browser.waitUntil(function () {
            return browser.isAlertOpen()
         }, {
             timeout: pageFunc.maxWait,
             timeoutMsg: 'Wait for alert to be present/prompt show'
         });
         browser.pause(2000); 
    }); 
 
    it('should accept prompt', () => {
        browser.acceptAlert()
        assert.equal('Confirm response: OK', pageFunc.getPromptTextResult(1))        
        browser.pause(2000); 
    });
    
    xit('should dismiss prompt', () => {
        browser.dismissAlert()
        assert.equal('Confirm response: Cancelled', pageFunc.getPromptTextResult(2))        
        browser.pause(2000); 
    }); 

    it('Wait for element to be visible', () => {
        browser.url('https://dineshvelhal.github.io/testautomation-playground/expected_conditions.html');
        browser.pause(2000);
        pageFunc.clickVisibilityTrigger() //click trigger button
        const btnClick = $('button#visibility_target')
        browser.waitUntil(function () {
           return btnClick.isDisplayed()
        }, {
            timeout: pageFunc.maxWait,
            timeoutMsg: 'Wait for element to be visible',
            interval:pageFunc.minWait
        });
        browser.pause(2000);
    });

    it('Wait for element to be Invisible', () => {
        browser.url('https://dineshvelhal.github.io/testautomation-playground/expected_conditions.html');
        pageFunc.clickInVisibilityTrigger()
        const btnClick = $('button#invisibility_target')
        const spinner_gone = $('#spinner_gone')
        browser.waitUntil(function () {
           return  spinner_gone.isDisplayed() && spinner_gone.getText() == 'Thank God that spinner is gone!' && !btnClick.isDisplayed()
        }, {
            timeout: pageFunc.maxWait,
            timeoutMsg: 'Wait for element to be Invisible',
            //interval:pageFunc.minWait
        });
        browser.pause(5000);
    });

    it('Wait for element to be enabled', () => {
        browser.url('https://dineshvelhal.github.io/testautomation-playground/expected_conditions.html');
        pageFunc.clickEnableTrigger()
        const btnClick = $('button#enabled_target')
        browser.waitUntil(function () {
           return btnClick.isEnabled()
        }, {
            timeout: pageFunc.maxWait,
            timeoutMsg: 'Wait for element to be enabled',
            //interval:pageFunc.minWait
        });
        browser.pause(5000);
    });

    //наверное не совсем верно, но сделала как проолжение предыдущего теста, чтобы не далеть оять сложное условие результата
    it('Wait for an attribute to contain certain text', () => {
        const btnClick = $('button#enabled_target')
        const btnAttr = btnClick.getAttribute('class')
        assert.equal(btnAttr, 'btn btn-success')
        browser.pause(5000);
    });

    it('Wait for Page Title to change', () => {
        browser.url('https://dineshvelhal.github.io/testautomation-playground/expected_conditions.html');
        pageFunc.clickTitleTrigger()
        browser.waitUntil(function () {
            return browser.getTitle() === 'My New Title!'
        }, {
            timeout: pageFunc.maxWait,
            timeoutMsg: 'expected title to be different after 5s',
            //interval:pageFunc.minWait
        });

        browser.pause(5000);
    });

    it('Wait for text/value to have specific values', () => {
    //    browser.url('https://dineshvelhal.github.io/testautomation-playground/expected_conditions.html');
        const waitForValue = $('input#wait_for_value')
        const waitForText = $('button#wait_for_text')
        pageFunc.clickTextValueTrigger()
        browser.waitUntil(function () {
            return (waitForValue.getValue() === 'Dennis Ritchie') && (waitForText.getText() === 'SUBMIT')
        }, {
            timeout: pageFunc.maxWait,
            timeoutMsg: 'Wait for text/value to have specific values Dennis Ritchie',
            //interval:pageFunc.minWait
        });

        browser.pause(5000);
    }); 

    it('Wait for frame to be available and then switch to it', () => {
        browser.url('https://dineshvelhal.github.io/testautomation-playground/expected_conditions.html');
        const waitForVisible = $('#frame')
        pageFunc.clickWaitFrameTrigger()
        browser.waitUntil(function () {
            return waitForVisible.isDisplayed()
        }, {
            timeout: pageFunc.maxWait,
            timeoutMsg: 'Wait for frame to be available',
            //interval:pageFunc.minWait
        });

        browser.pause(2000);
    });

    //продолжение предыдущего теста, так как ка обьединить не придумала, нам сначала ведь надо проверить, что загурзилось и только потом что кликатся
    it('switch to it', () => {
//        browser.url('https://dineshvelhal.github.io/testautomation-playground/expected_conditions.html');
        //const waitForVisible = $('#frame')
        pageFunc.iframe.waitForDisplayed();
        browser.switchToFrame(pageFunc.iframe);
        pageFunc.clickFrameInnerButton();
        assert.equal('CLICKED',pageFunc.buttonInnerFrame.getText())
        browser.pause(2000);
    });
 });