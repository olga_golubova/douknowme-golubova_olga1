const assert = require('assert');
const {URL} = require('url');

describe('dineshvelhal Keyboard Actions', () => {
    it ('mouse click',() => {
        browser.url('https://dineshvelhal.github.io/testautomation-playground/mouse_events.html');
        const blueBoxClickArea = $('#click_area');
        blueBoxClickArea.click();
        browser.pause(5000);
        const textArea = $('#click_type').getText();
        assert.equal(textArea,'Click'); 
        //assert.equal(textArea,'RClick'); //for test fail 
    });

    it ('mouse double click',() => {
        browser.url('https://dineshvelhal.github.io/testautomation-playground/mouse_events.html');
        const blueBoxClickArea = $('#click_area');
        blueBoxClickArea.doubleClick();
        browser.pause(5000);
        const textArea = $('#click_type').getText();
        assert.equal(textArea,'Double-Click'); 
        //assert.equal(textArea,'RClick'); //for test fail 
     });

    it ('mouse right click',() => {
        browser.url('https://dineshvelhal.github.io/testautomation-playground/mouse_events.html');
        const blueBoxClickArea = $('#click_area');
        blueBoxClickArea.click({ button: 'right' });
        browser.pause(10000);
        const textArea = $('#click_type').getText();
        assert.equal(textArea,'Right-Click'); 
        //assert.equal(textArea,'RClick'); //for test fail
    });

});