const assert = require('assert');

describe('dineshvelhal drag and drop', () => {
    it ('drag and drop', () => {
        browser.url('https://dineshvelhal.github.io/testautomation-playground/mouse_events.html');
 
        const elem = $('#drag_source');
        const target = $('#drop_target');
        elem.dragAndDrop(target, 10);

        const textArea = $('#drop_target h3').getText();
        assert.equal(textArea,'Drop is successful!');    
    });
});